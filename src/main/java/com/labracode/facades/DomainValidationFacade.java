package com.labracode.facades;

import com.labracode.services.WhoisHelperService;

public interface DomainValidationFacade {

    boolean isFreeDomain(String domain, WhoisHelperService whoisService);

}